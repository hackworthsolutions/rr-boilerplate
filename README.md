React + Redux boilerplate
=========================

This is a simple boilerplate for a React + Redux Frontend App.

The bundle is created using Webpack.

The CSS is compiled with **TBD**.

It transpiles ES6 using Babel. Feel free to use it to bootstrap any project.

Getting it to run
-----------------

To install the dependencies run:

`npm install`

or

`npm install --production`

(keep in mind that installing the production bundle won't install 
transpile-time dependencies such as babel-cli).

You should now be ready to launch the server by running:

`npm start`

It will listen to port `3333` in your loopback network by default.

Structure
---------

`index.html` is the HTML base.

`src/` has the source code:

* `src/index.js` is the entry point.
* `src/routes.js` has the React-router routes.
* `src/store/` has the redux store configuration for dev and production.
* `src/reducers/` has the reducers.
* `src/containers/` has the container, smart components.
* `src/components/` has the presentational, dumb components.

`dist/` has the babel-transpiled code.

