const webpack = require('webpack');
let plugins = [];

if(process.env.NODE_ENV === 'production') {
  plugins.push(new webpack.DefinePlugin({
    'process.env.NODE_ENV': JSON.stringify('production')
  }));
};

module.exports = {
  context: __dirname + "/src",
  entry: "./index.js",
  output: {
    path: __dirname + "/dist",
    filename: "bundle.js"
  },
  module: {
    preLoaders: [
      {
        test: /src.+\.jsx?$/,
        exclude: /node_modules/,
        loader: 'eslint'
      },
    ],
    loaders: [
      {
        test: /src.+\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel',
        query: {
          presets: ['react', 'es2015', 'stage-2']
        }
      },
      {
        test: /\.css$/,
        loader: "style-loader!css-loader?modules"
      },
      {
        test: /\.png$/,
        loader: "url-loader?limit=100000"
      },
      {
        test: /\.jpg$/,
        loader: "file-loader"
      }
    ]
  },
  eslint: {
    parserOptions: {
      ecmaVersion: 6,
      sourceType: 'module',
      ecmaFeatures: {
        jsx: true,
        modules: true,
        experimentalObjectRestSpread: true
      },
    },
  },
  plugins: plugins,
  devServer: {
    inline: true,
    publicPath: '/dist/',
    historyApiFallback: true,
    port: 3333
  }
};

