import { createStore, applyMiddleware, combineReducers } from 'redux';
import { routerMiddleware, routerReducer } from 'react-router-redux'
import { browserHistory } from 'react-router'
import thunkMiddleware from 'redux-thunk';
import reducers from '../reducers';

const historyMiddleware = routerMiddleware( browserHistory );

export default function configureStore(initialState) {
  return createStore(
    combineReducers({
      ...reducers,
      routing: routerReducer
    }),
    initialState,
    applyMiddleware(
      thunkMiddleware,  // lets us dispatch() functions
      historyMiddleware // adapt react-router
    )
  );
};

