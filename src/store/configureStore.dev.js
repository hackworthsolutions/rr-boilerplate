import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import { routerMiddleware, routerReducer } from 'react-router-redux'
import { browserHistory } from 'react-router'
import { persistState } from 'redux-devtools';
import thunkMiddleware from 'redux-thunk';
import createLogger from 'redux-logger';
import reducers from '../reducers';
import DevTools from '../components/DevTools';

const loggerMiddleware = createLogger();
const historyMiddleware = routerMiddleware( browserHistory );

function getDebugSessionKey() {
  // TODO: You can write custom logic here!
  // By default we try to read the key from ?debug_session=<key> in the address bar
  const matches = window.location.href.match(/[?&]debug_session=([^&]+)\b/);
  return (matches && matches.length > 0)? matches[1] : null;
}

export default function configureStore(initialState) {
  const store = createStore(
    combineReducers({
      ...reducers,
      routing: routerReducer
    }),
    initialState,
    compose(
      applyMiddleware(
        thunkMiddleware,  // lets us dispatch() functions
        loggerMiddleware, // neat middleware that logs actions
        historyMiddleware // adapt react-router
      ),
      DevTools.instrument(),
      persistState(getDebugSessionKey())
    )
  );


  if (module.hot) {
    module.hot.accept('../reducers', () =>
                      store.replaceReducer(require('../reducers')/*.default if you use Babel 6+ */)
                     );
  }

  return store;
};

