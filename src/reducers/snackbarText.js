export function snackbarText(state = '', action) {
  switch (action.type) {
    case 'OPEN_SNACKBAR':
      return action.message;
    case 'CLOSE_SNACKBAR':
      return '';
    default:
      return state
  }
}

