import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import Snackbar from 'material-ui/Snackbar';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import NavigationMenu from 'material-ui/svg-icons/navigation/menu';

import { closeSnackbar } from '../actions/base';

const actionCreators = { closeSnackbar};

const AppContainer = ({
  url,
  children,
  snackbarText,
  closeSnackbar
}) =>
  <MuiThemeProvider muiTheme={getMuiTheme()}>
    <div>
      <AppBar
        title="Title"
        iconElementLeft={<IconButton><NavigationMenu /></IconButton>}
        iconElementRight={
          <IconMenu
            iconButtonElement={
              <IconButton><MoreVertIcon /></IconButton>
            }
            targetOrigin={{horizontal: 'right', vertical: 'top'}}
            anchorOrigin={{horizontal: 'right', vertical: 'top'}}
            >
            <MenuItem primaryText="Refresh" />
            <MenuItem primaryText="Help" />
            <MenuItem primaryText="Sign out" />
          </IconMenu>
        }
      />
      {children}
      <Snackbar
        open={!!snackbarText}
        message={snackbarText}
        autoHideDuration={2000}
        onRequestClose={closeSnackbar}
      />
    </div>
  </MuiThemeProvider>

export const App = connect(
  state => ({
    snackbarText: state.snackbarText,
    url: state.routing.locationBeforeTransitions.pathname
  }),
  dispatch => bindActionCreators(actionCreators, dispatch)
)(AppContainer);
