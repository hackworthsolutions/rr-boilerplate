import React, { Component } from 'react';
import { Link } from 'react-router'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';

/* The only action we support for now... */
const actionCreators = { push };

const AboutContainer = ({ push }) =>
  <article>
    <h1>About</h1>
    <p>Some about text...</p>
    <p>Some more about text...</p>
    <p><Link to="/">Go to "Home"</Link></p>
    <p><a onClick={ e => push("/") }>Go to "Home" too</a></p>
  </article>

export const About = connect(
  state => state,
  dispatch => bindActionCreators(actionCreators, dispatch)
)(AboutContainer)

