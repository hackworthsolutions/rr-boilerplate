import React, { Component } from 'react';
import { Link } from 'react-router'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import * as baseActions from '../actions/base';

/* The only action we support for now... */
const actionCreators = { push, ...baseActions };

const HomeContainer = ({
  push,
  openSnackbar
}) =>
  <article>
    <h1>Home page</h1>
    <p>Some text...</p>
    <p>Some more text...</p>
    <p><a onClick={ e => openSnackbar('Hi there!') }>Snackbar something</a></p>
    <p><Link to="/about">Go to "About"</Link></p>
    <p><a onClick={ e => push("/about") }>Go to "About" too</a></p>
  </article>

export const Home = connect(
  state => state,
  dispatch => bindActionCreators(actionCreators, dispatch)
)(HomeContainer)

