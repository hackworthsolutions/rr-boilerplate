import React from 'react'
import ReactDOM from 'react-dom'
import { browserHistory } from 'react-router'
import { syncHistoryWithStore } from 'react-router-redux'
import injectTapEventPlugin from 'react-tap-event-plugin'
import configureStore from './store/configureStore';
import routes from './routes';
import Root from './containers/Root';

// TODO: Pass initial state
const store = configureStore();

const history = syncHistoryWithStore( browserHistory, store );

// Needed for onTouchTap
// http://stackoverflow.com/a/34015469/988941
injectTapEventPlugin();

ReactDOM.render(
  <Root store={store} history={history} />,
	document.getElementById('root')
);

