export const openSnackbar = (message) => ({
  type: 'OPEN_SNACKBAR',
  message
});

export const closeSnackbar = () => ({
  type: 'CLOSE_SNACKBAR'
});
